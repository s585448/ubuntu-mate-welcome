#
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-03-10 10:57+0000\n"
"PO-Revision-Date: 2016-03-10 10:58+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language-Team: : LANGUAGE\n"
"Report-Msgid-Bugs-To: you@example.com\n"
"X-Generator: Poedit 1.8.4\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en_GB\n"

#: index.html:14
msgid "Main Menu"
msgstr "Main Menu"

#: index.html:18
msgid "Subscribe to Welcome updates"
msgstr "Subscribe to Welcome updates"

#: index.html:21
msgid "Welcome will automatically restart in a few moments..."
msgstr "Welcome will automatically restart in a few moments..."

#: index.html:36
msgid "Choose an option to discover your new operating system."
msgstr "Choose an option to discover your new operating system."

#: index.html:44
msgid "Introduction"
msgstr "Introduction"

#: index.html:45
msgid "Features"
msgstr "Features"

#: index.html:46
msgid "Getting Started"
msgstr "Getting Started"

#: index.html:47
msgid "Installation Help"
msgstr "Installation Help"

#: index.html:52
msgid "Get Involved"
msgstr "Get Involved"

#: index.html:53
msgid "Shop"
msgstr "Shop"

#: index.html:54
msgid "Donate"
msgstr "Donate"

#: index.html:62
msgid "Community"
msgstr "Community"

#: index.html:63
msgid "Chat Room"
msgstr "Chat Room"

#: index.html:64
msgid "Software"
msgstr "Software"

#: index.html:65
msgid "Install Now"
msgstr "Install Now"

#: index.html:72
msgid "Raspberry Pi Information"
msgstr "Raspberry Pi Information"

#: index.html:85
msgid "Open Welcome when I log on."
msgstr "Open Welcome when I log on."
